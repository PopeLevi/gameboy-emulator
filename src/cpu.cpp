/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "cpu.hpp"
#include "interrupt.hpp"
#include "memory.hpp"
#include "timer.hpp"
#include "asm.hpp"

namespace GameboyEmu
{
    CPU::CPU()
    {
	Memory::init();

	// Default values, apparently
	AF = 0x01B0;
	BC = 0x0013;
	DE = 0x00D8;
	HL = 0x014D;

	PC = 0x0100; SP = 0xFFFE;

	cycles = 0;
	halt = false;

	intHandler = new InterruptHandler;
	intHandler->set_cpu(this);

	Timer::init(intHandler);
    }

    CPU::~CPU()
    {
	delete intHandler;
    }

    void CPU::run_cycle()
    {
	if (halt)
	{
	    cycles += 4;
	    return;
	}

	uint32_t flags = 0;

	switch (Memory::get_byte(PC))
	{
	    case 0x00: // NOP
		PC++;
		cycles += 4;
		break;
		
	    case 0x01: // LD BC, D16
		BC = Memory::get_word(PC + 1);
		PC += 3;
		cycles += 12;
		break;
		
	    case 0x02: // LD (BC), A
		Memory::write_byte(BC, A);
		PC++;
		cycles += 8;
		break;
		
	    case 0x03: // INC BC
		BC++;
		
		PC++;
		cycles += 8;
		break;
		
	    case 0x04: // INC B
		B++;
		
		if (B == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((B & 0x0F) == 0x00) SET_FLAG(H);
		else CLEAR_FLAG(H);

		CLEAR_FLAG(N);

		PC++;
		cycles += 4;

		break;
		
	    case 0x05: // DEC B
		B--;

		if (B == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

	        if ((B & 0x0F) == 0x0F) SET_FLAG(H);
		else CLEAR_FLAG(H);

	        SET_FLAG(N);

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x06: // LD B, D8
		B = Memory::get_byte(PC + 1);
		
		PC += 2;
		cycles += 8;
		
		break;
		
	    case 0x07: // RLCA
		flags = _op_rlc(&A, GET_FLAG(C));
		
		if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
		else CLEAR_FLAG(C);

		CLEAR_FLAG(H);
		CLEAR_FLAG(N);
		CLEAR_FLAG(Z);

		PC++;
		cycles += 4;
		
		break;
	    
	    case 0x08: // LD (A16), SP
		Memory::write_word(Memory::get_word(PC + 1), SP);
		PC += 3;
		cycles += 20;
		
		break;
		
	    case 0x09: // ADD HL, BC
		flags = _op_add16(&HL, BC);
		if (GET_OP_FLAG(H, flags)) SET_FLAG(H);
		else CLEAR_FLAG(H);

		if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
		else CLEAR_FLAG(C);

		CLEAR_FLAG(N);

		PC++;
		cycles += 8;
		
		break;

	    case 0x0A: // LD A, (BC)
		A = Memory::get_byte(BC);

		PC++;
		cycles += 8;
		
		break;
		
	    case 0x0B: // DEC BC
		BC--;
		
		PC++;
		cycles += 8;

		break;
		
	    case 0x0C: // INC C
		C++;
		
		if (C == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((C & 0x0F) == 0x00) SET_FLAG(H);
		else CLEAR_FLAG(H);

		CLEAR_FLAG(N);

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x0D: // DEC C
		C--;
		
		if (C == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((C & 0x0F) == 0x0F) SET_FLAG(H);
		else CLEAR_FLAG(H);

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x0E: // LD C, D8
		C = Memory::get_byte(PC + 1);

		PC += 2;
		cycles += 8;
		
		break;
		
	    case 0x0F: // RRCA
		flags = _op_rrc(&A, GET_FLAG(C));
		if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
		else CLEAR_FLAG(C);

		PC++;
		cycles += 4;
		
		break;

	    case 0x10: // STOP
		cycles += 4;
	    
	    case 0x11: // LD DE, D16
		DE = Memory::get_word(PC + 1);
		
		PC += 3;
		cycles += 12;
		
		break;
		
	    case 0x12: // LD (DE), A
		Memory::write_byte(DE, A);

		PC++;
		cycles += 8;
		
		break;
		
	    case 0x13: // INC DE
		DE++;
		
		PC++;
		cycles += 8;

		break;
		
	    case 0x14: // INC D
		D++;

		if (D == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((D & 0x0F) == 0x00) SET_FLAG(H);
		else CLEAR_FLAG(Z);

		CLEAR_FLAG(N);

		PC++;
		cycles += 4;
		    
		break;
		
	    case 0x15: // DEC D
		D--;

		if (D == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((D & 0x0F) == 0x0F) SET_FLAG(H);
		else CLEAR_FLAG(H);

		SET_FLAG(N);

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x16: // LD D, D8
		D = Memory::get_byte(PC + 1);

		PC += 2;
		cycles += 8;
		
		break;
		
	    case 0x17: // RLA
		flags = _op_rl(&A);
		if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
		else CLEAR_FLAG(C);

		CLEAR_FLAG(Z);
		CLEAR_FLAG(N);
		CLEAR_FLAG(H);
		
		PC++;
		cycles += 4;
		
		break;
	    
	    case 0x18: // JR, R8
		PC += (int8_t)Memory::get_byte(PC + 1);

		cycles += 12;
		
		break;
		
	    case 0x19: // ADD HL, DE
		flags = _op_add16(&HL, DE);
		if (GET_OP_FLAG(H, flags)) SET_FLAG(H);
		else CLEAR_FLAG(H);

		if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
		else CLEAR_FLAG(C);

		CLEAR_FLAG(N);

		PC++;
		cycles += 8;

		break;
	    
	    case 0x1A: // LD A, (DE)
		A = Memory::get_byte(DE);

		PC++;
		cycles += 8;

		break;
		
	    case 0x1B: // DEC DE
		DE--;

		PC++;
		cycles += 8;
		
		break;
		
	    case 0x1C: // INC E
		E++;

		if (E == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((E & 0x0F) == 0x00) SET_FLAG(H);
		else CLEAR_FLAG(H);

		CLEAR_FLAG(N);

		PC++;
		cycles += 4;

		break;
		
	    case 0x1D: // DEC E
		E--;

		if (E == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((E & 0x0F) == 0x0F) SET_FLAG(H);
		else CLEAR_FLAG(H);

		SET_FLAG(N);

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x1E: // LD E, D8
		E = Memory::get_byte(PC + 1);

		PC += 2;
		cycles += 8;

		break;
		
	    case 0x1F: // RRA
		flags = _op_rr(&A);

		if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
		else CLEAR_FLAG(C);

		CLEAR_FLAG(Z);
		CLEAR_FLAG(N);
		CLEAR_FLAG(H);
		
		PC++;
		cycles += 4;
		
		break;
		
	    case 0x20: // JR NZ, R8
		if (!GET_FLAG(Z))
		{
		    PC += (int8_t)Memory::get_byte(PC + 1);
		    cycles += 12;
		} else {
		    PC += 2;
		    cycles += 8;
		}

		break;
		
	    case 0x21: // LD HL, D16
		HL = Memory::get_word(PC + 1);

		PC += 2;
		cycles += 12;

		break;
		
	    case 0x22: // LDI (HL), A
		Memory::write_byte(HL++, A);

		PC++;
		cycles += 8;

		break;
		
	    case 0x23: // INC HL
		HL++;

		PC++;
		cycles += 8;

		break;
		
	    case 0x24: // INC H
		H++;

		if (H == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((H & 0x0F) == 0x00) SET_FLAG(H);
		else CLEAR_FLAG(H);

		CLEAR_FLAG(N);

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x25: // DEC H
		H--;

		if (H == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);
		
		if ((H & 0x0F) == 0x0F) SET_FLAG(H);
		else CLEAR_FLAG(H);

		SET_FLAG(N);
		
		PC++;
		cycles += 4;

		break;
		
	    case 0x26: // LD H, D8
		H = Memory::get_byte(PC + 1);

		PC += 2;
		cycles += 8;
		
		break;

	    case 0x27: // DAA
	    {
		byte correctionFactor = 0x00;
		
		if (A > 0x99 || GET_FLAG(C)) correctionFactor |= 0x06 << 4;
	        else CLEAR_FLAG(C);

		if ((A & 0x0F) > 0x09 || GET_FLAG(H)) correctionFactor |= 0x06;

		if (GET_FLAG(N))
		{
		    if ((A & 0x0F) - correctionFactor > (A & 0x0F)) SET_FLAG(H);
		    else CLEAR_FLAG(H);
		    
		    A -= correctionFactor;   
		} else {
		    if ((A & 0x0F) + correctionFactor > (A & 0x0F)) SET_FLAG(H);
		    else CLEAR_FLAG(H);
		    
		    A += correctionFactor;
		}		

		break;
	    }
	    
	    case 0x28: // JR Z, R8
		if (GET_FLAG(Z))
		{
		    PC += (int8_t)Memory::get_byte(PC + 1);
		    cycles += 12;
		} else {
		    PC += 2;
		    cycles += 8;
		}
		
		break;
		
	    case 0x29: // ADD HL, HL
		flags = _op_add16(&HL, HL);
		if (GET_OP_FLAG(H, flags)) SET_FLAG(H);
		else CLEAR_FLAG(H);

		if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
		else CLEAR_FLAG(C);

		CLEAR_FLAG(N);

		PC++;
		cycles += 8;

		break;
		
	    case 0x2A: // LDI A, (HL)
		A = Memory::get_byte(HL++);

		PC++;
		cycles += 8;

		break;
		
	    case 0x2B: // DEC HL
		HL--;

		PC++;
		cycles += 8;

		break;
		
	    case 0x2C: // INC L
		L++;

		if (L == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((L & 0x0F) ==  0x00) SET_FLAG(H);
		else CLEAR_FLAG(H);

		CLEAR_FLAG(N);

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x2D: // DEC L
		L--;
		
		if (L == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((L & 0x0F) == 0x0F) SET_FLAG(H);
		else CLEAR_FLAG(H);

		SET_FLAG(N);

		PC++;
		cycles += 4;

		break;
		
	    case 0x2E: // LD L, D8
		L = Memory::get_byte(PC + 1);

		PC += 2;
		cycles += 8;

		break;
		
	    case 0x2F: // CPL
		A = ~A;

		PC++;
		cycles += 4;

		break;
		
	    case 0x30: // JR NC, R8
		if (!GET_FLAG(C))
		{
		    PC += (int8_t)Memory::get_byte(PC + 1);
		    cycles += 12;
		} else {
		    PC += 2;
		    cycles += 8;
		}

		break;
		
	    case 0x31: // LD SP, D16
		SP = Memory::get_word(PC + 1);

		PC += 3;
		cycles += 12;

		break;
		
	    case 0x32: // LDD (HL), A
		Memory::write_byte(HL, A);
		HL--;

		PC++;
		cycles += 8;

		break;
		
	    case 0x33: // INC SP
		SP++;

		PC++;
		cycles += 8;

		break;
		
	    case 0x34: // INC (HL)
		Memory::write_byte(HL, Memory::get_byte(HL) + 1);

		if (Memory::get_byte(HL) == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((Memory::get_byte(HL) & 0x0F) == 0x00) SET_FLAG(H);
		else CLEAR_FLAG(H);

		CLEAR_FLAG(N);

		
		PC++;
		cycles += 12;

		break;
		
	    case 0x35: // DEC (HL)
		Memory::write_byte(HL, Memory::get_byte(HL) - 1);

		if (Memory::get_byte(HL) == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((Memory::get_byte(HL) & 0x0F) == 0x0F) SET_FLAG(H);
		else CLEAR_FLAG(H);

		SET_FLAG(N);
		
		PC++;
		cycles += 12;
		
		break;
		
	    case 0x36: // LD (HL), D8
		Memory::write_byte(HL, Memory::get_byte(PC + 1));

		PC += 2;
		cycles += 12;
		
		break;
		
	    case 0x37: // SCF
		SET_FLAG(C);

		PC++;
		cycles += 4;

		break;
		
	    case 0x38: // JR C, R8
		if (GET_FLAG(C))
		{
		    PC += (int8_t)Memory::get_byte(PC + 1);
		    cycles += 12;
		} else {
		    PC += 2;
		    cycles += 8;
		}

		break;
		
	    case 0x39: // ADD HL, SP
		flags = _op_add16(&HL, SP);
		if (GET_OP_FLAG(H, flags)) SET_FLAG(H);
		else CLEAR_FLAG(H);

		if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
		else CLEAR_FLAG(C);

		CLEAR_FLAG(N);
		
		PC++;
		cycles += 8;

		break;
		
	    case 0x3A: // LDD A, (HL)
		A = Memory::get_byte(HL);
		HL--;
		
		PC++;
		cycles += 8;

		break;
		
	    case 0x3B: // DEC SP
		SP--;

		PC++;
		cycles += 4;

		break;
		
	    case 0x3C: // INC A
		A++;

		if (A == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((A & 0x0F) == 0x00) SET_FLAG(H);
		else CLEAR_FLAG(H);

		CLEAR_FLAG(N);

		PC++;
		cycles += 4;

		break;
		
	    case 0x3D: // DEC A
		A--;

		if (A == 0) SET_FLAG(Z);
		else CLEAR_FLAG(Z);

		if ((A & 0x0F) == 0x0F) SET_FLAG(H);
		else CLEAR_FLAG(H);

		SET_FLAG(N);

		PC++;
		cycles += 4;

		break;
		
	    case 0x3E: // LD A, D8
		A = Memory::get_byte(PC + 1);

		PC += 2;
		cycles += 8;

		break;
		
	    case 0x3F: // CCF
		CLEAR_FLAG(N);

		if (GET_FLAG(C)) CLEAR_FLAG(C);
		else SET_FLAG(C);

		if (GET_FLAG(H)) CLEAR_FLAG(H);
		else SET_FLAG(H);

		PC++;
		cycles += 4;

		break;
		
	    case 0x40: // LD B, B
		PC++;
		cycles += 4;

		break;
		
	    case 0x41: // LD B, C
		B = C;

		PC++;
		cycles += 4;

		break;
		
	    case 0x42: // LD B, D
		B = D;

		PC++;
		cycles += 4;

		break;
		
	    case 0x43: // LD B, E
		B = E;

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x44: // LD B, H
		B = H;
		
		PC++;
		cycles += 4;
		
		break;
		
	    case 0x45: // LD B, L
		B = L;

		PC++;
		cycles += 4;

		break;
		
	    case 0x46: // LD B, (HL)
		B = Memory::get_byte(HL);

		PC++;
		cycles += 8;

		break;
		
	    case 0x47: // LD B, A
		B = A;

		PC++;
		cycles += 4;

		break;
		
	    case 0x48: // LD C, B
		C = B;

		PC++;
		cycles += 4;

		break;
		
	    case 0x49: // LD C, C
		PC++;
		cycles += 4;

		break;
		
	    case 0x4A: // LD C, D
		C = D;

		PC++;
		cycles += 4;

		break;
		
	    case 0x4B: // LD C, E
		C = E;

		PC++;
		cycles += 4;

		break;
		
	    case 0x4C: // LD C, H
		C = H;

		PC++;
		cycles += 4;

		break;
		
	    case 0x4D: // LD C, L
		C = L;

		PC++;
		cycles += 4;

		break;
		
	    case 0x4E: // LD C, (HL)
		C = Memory::get_byte(HL);

		PC++;
		cycles += 8;

		break;
		
	    case 0x4F: // LD C, A
		C = A;

		PC++;
		cycles += 4;

		break;
		
	    case 0x50: // LD D, B
		D = B;

		PC++;
		cycles += 4;

		break;
		
	    case 0x51: // LD D, C
		D = C;

		PC++;
		cycles += 4;

		break;
		
	    case 0x52: // LD D, D
		PC++;
		cycles += 4;

		break;
		
	    case 0x53: // LD D, E
		D = E;

		PC++;
		cycles += 4;

		break;
		
	    case 0x54: // LD D, H
		D = H;

		PC++;
		cycles += 4;

		break;
		
	    case 0x55: // LD D, L
		D = L;

		PC++;
		cycles += 4;

		break;
		
	    case 0x56: // LD D, (HL)
		D = Memory::get_byte(HL);

		PC++;
		cycles += 8;

		break;
		
	    case 0x57: // LD D, A
		D = A;

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x58: // LD E, B
		E = B;

		PC++;
		cycles += 4;

		break;
		
	    case 0x59: // LD E, C
		E = C;

		PC++;
		cycles += 4;

		break;
		
	    case 0x5A: // LD E, D
		E = D;

		PC++;
		cycles += 4;

		break;
		
	    case 0x5B: // LD E, E
		PC++;
		cycles += 4;

		break;
		
	    case 0x5C: // LD E, H
		E = H;

		PC++;
		cycles += 4;

		break;
		
	    case 0x5D: // LD E, L
		E = L;

		PC++;
		cycles += 4;

		break;
		
	    case 0x5E: // LD E, (HL)
		E = Memory::get_byte(HL);

		PC++;
		cycles += 4;

		break;
		
	    case 0x5F: // LD E, A
		E = A;

		PC++;
		cycles += 4;

		break;
		
	    case 0x60: // LD H, B
		H = B;

		PC++;
		cycles += 4;

		break;
		
	    case 0x61: // LD H, C
		H = C;

		PC++;
		cycles += 4;

		break;
		
	    case 0x62: // LD H, D
		H = D;

		PC++;
		cycles += 4;

		break;
		
	    case 0x63: // LD H, E
		H = E;

		PC++;
		cycles += 4;

		break;
		
	    case 0x64: // LD H, H
		PC++;
		cycles += 4;
		
		break;
		
	    case 0x65: // LD H, L
		H = L;

		PC++;
		cycles += 4;

		break;
		
	    case 0x66: // LD H, (HL)
		H = Memory::get_byte(HL);

		PC++;
		cycles += 4;

		break;
		
	    case 0x67: // LD H, A
		H = A;

		PC++;
		cycles += 4;

		break;
		
	    case 0x68: // LD L, B
		L = B;

		PC++;
		cycles += 4;

		break;
		
	    case 0x69: // LD L, C
		L = C;

		PC++;
		cycles += 4;

		break;
		
	    case 0x6A: // LD L, D
		L = D;

		PC++;
		cycles += 4;

		break;
		
	    case 0x6B: // LD L, E
		L = E;

		PC++;
		cycles += 4;
		
		break;
		
	    case 0x6C: // LD L, H
		L = H;

		PC++;
		cycles += 4;

		break;
		
	    case 0x6D: // LD L, L
		PC++;
		cycles += 4;

		break;
		
	    case 0x6E: // LD L, (HL)
		L = Memory::get_byte(HL);

		PC++;
		cycles += 8;

		break;
		
	    case 0x6F: // LD L, A
		L = A;

		PC++;
		cycles += 4;

		break;
		
	    case 0x70: // LD (HL), B
		Memory::write_byte(HL, B);

		PC++;
		cycles += 8;

		break;
		
	    case 0x71: // LD (HL), C
		Memory::write_byte(HL, C);

		PC++;
		cycles += 8;
		
		break;
		
	    case 0x72: // LD (HL), D
		Memory::write_byte(HL, D);

		PC++;
		cycles += 8;

		break;
		
	    case 0x73: // LD (HL), E
		Memory::write_byte(HL, E);

		PC++;
		cycles += 8;

		break;
		
	    case 0x74: // LD (HL), H
		Memory::write_byte(HL, H);

		PC++;
		cycles += 8;

		break;
		
	    case 0x75: // LD (HL), L
		Memory::write_byte(HL, L);

		PC++;
		cycles += 8;

		break;
		
	    case 0x76: // HALT
		halt = true;
		
		PC++;
		cycles += 4;

		break;
		
	    case 0x77: // LD (HL), A
		Memory::write_byte(HL, A);

		PC++;
		cycles += 8;

		break;
		
	    case 0x78: // LD A, B
		A = B;

		PC++;
		cycles += 4;

		break;
		
	    case 0x79: // LD A, C
		A = C;

		PC++;
		cycles += 4;

		break;
		
	    case 0x7A: // LD A, D
		A = D;

		PC++;
		cycles += 4;

		break;
		
	    case 0x7B: // LD A, E
		A = E;

		PC++;
		cycles += 4;

		break;
		
	    case 0x7C: // LD A, H
		A = H;

		PC++;
		cycles += 4;

		break;
		
	    case 0x7D: // LD A, L
		A = L;

		PC++;
		cycles += 4;

		break;
		
	    case 0x7E: // LD A, (HL)
		A = Memory::get_byte(HL);

		PC++;
		cycles += 4;

		break;
		
	    case 0x7F: // LD A, A
		PC++;
		cycles += 4;

		break;
		
	    case 0x80: // ADD A, B
		this->add_op(&B);
		
		break;
		
	    case 0x81: // ADD A, C
		this->add_op(&C);
		
		break;
		
	    case 0x82: // ADD A, D
		this->add_op(&D);
		
		break;
		
	    case 0x83: // ADD A, E
		this->add_op(&E);
		
		break;
		
	    case 0x84: // ADD A, H
		this->add_op(&H);
		
		break;
		
	    case 0x85: // ADD A, L
		this->add_op(&L);
		
		break;
		
	    case 0x86: // ADD A, (HL)
		this->add_op(Memory::get_byte_ptr(HL));
		
		break;
		
	    case 0x87: // ADD A, A
		this->add_op(&A);
		
		break;
		
	    case 0x88: // ADC A, B
		this->sbc_op(&B);

		break;
		
	    case 0x89: // ADC A, C
		this->sbc_op(&C);

		break;
		
	    case 0x8A: // ADC A, D
		this->adc_op(&D);
		
		break;
		
	    case 0x8B: // ADC A, E
		this->adc_op(&E);
		
		break;
		
	    case 0x8C: // ADC A, H
		this->adc_op(&H);
		
		break;
		
	    case 0x8D: // ADC A, L
		this->adc_op(&L);
		
		break;
		
	    case 0x8E: // ADC A, (HL)
		this->adc_op(Memory::get_byte_ptr(HL));
		
		break;
		
	    case 0x8F: // ADC A, A
		this->adc_op(&A);
		
		break;
		
	    case 0x90: // SUB B
		this->sub_op(&B);
		
		break;
		
	    case 0x91: // SUB C
		this->sub_op(&C);
		
		break;
		
	    case 0x92: // SUB D
		this->sub_op(&D);

		break;
		
	    case 0x93: // SUB E
		this->sub_op(&E);

		break;
		
	    case 0x94: // SUB H
		this->sub_op(&H);

		break;
		
	    case 0x95: // SUB L
		this->sub_op(&L);

		break;
		
	    case 0x96: // SUB (HL)
		this->sub_op(Memory::get_byte_ptr(HL));

		cycles += 4; // This opcode takes 8 cycles. sub_op() provides 4 of these

		break;
	    
	    case 0x97: // SUB A
		this->sub_op(&A);

		break;
		
	    case 0x98: // SBC B
		this->sbc_op(&B);
		
		break;
		
	    case 0x99: // SBC C
		this->sbc_op(&C);

		break;
		
	    case 0x9A: // SBC D
		this->sbc_op(&D);

		break;
		
	    case 0x9B: // SBC E
		this->sbc_op(&E);

		break;
		
	    case 0x9C: // SBC H
		this->sbc_op(&H);

		break;
		
	    case 0x9D: // SBC L
		this->sbc_op(&L);

		break;
		
	    case 0x9E: // SBC (HL)
		this->sbc_op(Memory::get_byte_ptr(HL));

		break;
	    
	    case 0x9F: // SBC A
		this->sbc_op(&A);

		break;
		
	    case 0xA0: // AND B
		this->and_op(&B);

		break;
		
	    case 0xA1: // AND C
		this->and_op(&C);

		break;
		
	    case 0xA2: // AND D
		this->and_op(&D);

		break;
		
	    case 0xA3: // AND E
		this->and_op(&E);

		break;
		
	    case 0xA4: // AND H
		this->and_op(&H);

		break;
		
	    case 0xA5: // AND L
		this->and_op(&L);

		break;
		
	    case 0xA6: // AND (HL)
		this->and_op(Memory::get_byte_ptr(HL));
		
		break;
	    
	    case 0xA7: // AND A
		PC++;
		cycles += 4;

		break;
		
	    case 0xA8: // XOR B
		this->xor_op(&B);

		break;
		
	    case 0xA9: // XOR C
		this->xor_op(&C);

		break;
		
	    case 0xAA: // XOR D
		this->xor_op(&D);

		break;
		
	    case 0xAB: // XOR E
		this->xor_op(&E);

		break;
		
	    case 0xAC: // XOR H
		this->xor_op(&H);

		break;
		
	    case 0xAD: // XOR L
		this->xor_op(&L);

		break;
		
	    case 0xAE: // XOR (HL)
		this->xor_op(Memory::get_byte_ptr(HL));
		
		break;
	    
	    case 0xAF: // XOR A
		this->xor_op(&A);
		
		break;
		
	    case 0xB0: // OR B
		this->or_op(&B);

		break;
		
	    case 0xB1: // OR C
		this->or_op(&C);

		break;
		
	    case 0xB2: // OR D
		this->or_op(&D);

		break;
		
	    case 0xB3: // OR E
		this->or_op(&E);

		break;
		
	    case 0xB4: // OR H
		this->or_op(&H);

		break;
		
	    case 0xB5: // OR L
		this->or_op(&L);

		break;
		
	    case 0xB6: // OR (HL)
		this->or_op(Memory::get_byte_ptr(HL));
		
		break;
	    
	    case 0xB7: // OR A
		PC++;
		cycles += 4;
		
		break;
		
	    case 0xB8: // CP B
		this->cp_op(&B);
		
		break;
		
	    case 0xB9: // CP C
		this->cp_op(&C);

		break;
	    
	    case 0xBA: // CP D
		this->cp_op(&D);
		
		break;
		
	    case 0xBB: // CP E
		this->cp_op(&E);

		break;
		
	    case 0xBC: // CP H
		this->cp_op(&H);

		break;
		
	    case 0xBD: // CP L
		this->cp_op(&L);

		break;
		
	    case 0xBE: // CP (HL)
		this->cp_op(Memory::get_byte_ptr(HL));

		break;
	    
	    case 0xBF: // CP A
		this->cp_op(&A);

		break;
		
	    case 0xC0: // RET NZ
		if (!GET_FLAG(Z))
		{
		    PC = Memory::get_word(SP);
		    SP += 2;

		    cycles += 20;
		} else {
		    PC++;
		    cycles += 8;
		}

		break;
		
	    case 0xC1: // POP BC
		BC = Memory::get_word(SP);
		SP += 2;

		PC++;
		cycles += 12;
		
		break;
		
	    case 0xC2: // JP NZ, A16
		if (!GET_FLAG(Z))
		{
		    PC = Memory::get_word(PC + 1);
		    cycles += 16;
		} else {
		    PC += 3;
		    cycles += 12;
		}

		break;
		
	    case 0xC3: // JP A16
		PC = Memory::get_word(PC + 1);

		cycles += 16;

		break;
		
	    case 0xC4: // CALL NZ, A16
		if (!GET_FLAG(Z))
		{
		    SP -= 2;
		    PC += 3;
		    Memory::write_word(SP, PC);
		    PC = Memory::get_word(PC + 1);
		    
		    cycles += 24;
		} else {
		    cycles += 12;
		    PC += 3;
		}

		break;
		
	    case 0xC5: // PUSH BC
		SP -= 2;
		Memory::write_word(SP, BC);

		PC++;
		cycles += 16;

		break;
		
	    case 0xC6: // ADD D8
		this->add_op(Memory::get_byte_ptr(PC + 1));

		PC++;
		cycles += 4;
		
		break;
		
	    case 0xC7: // RST 00h
		SP -= 2;
		PC++;
		Memory::write_word(SP, PC);
		
		PC = 0x0000;
		
		cycles += 16;

		break;
		
	    case 0xC8: // RET Z
		if (GET_FLAG(Z))
		{
		    PC = Memory::get_word(SP);
		    SP += 2;
		    
		    cycles += 20;
		} else {
		    PC++;
		    cycles += 8;
		}

		break;
		
	    case 0xC9: // RET
		PC = Memory::get_word(SP);
		SP += 2;
		cycles += 16;
		
		break;
		
	    case 0xCA: // JP Z, A16
		if (GET_FLAG(Z))
		{
		    PC = Memory::get_word(PC + 1);
		    cycles += 16;
		} else {
		    PC += 3;
		    cycles += 12;
		}

		break;

	    case 0xCB:
		this->exec_cb(Memory::get_byte(PC + 1));

		break;
		
	    case 0xCC: // CALL Z, A16
		if (GET_FLAG(Z))
		{
		    SP -= 2;
		    PC += 3;
		    Memory::write_word(SP, PC);
		    PC = Memory::get_word(PC + 1);

		    cycles += 24;
		} else {
		    PC += 3;
		    cycles += 12;
		}

		break;
		
	    case 0xCD: // CALL A16
		SP -= 2;
		Memory::write_word(SP, PC + 3);
		PC = Memory::get_word(PC + 1);
		
		cycles += 24;

		break;
		
	    case 0xCE: // ADC D8
		this->adc_op(Memory::get_byte_ptr(PC + 1));

		PC++;
		cycles += 4;

		break;
		
	    case 0xCF: // RST 08h
		SP -= 2;
		PC++;
		Memory::write_word(SP, PC);

		PC = 0x0008;
		cycles += 16;
		
		break;
		
	    case 0xD0: // RET NC
		if (!GET_FLAG(N))
		{
		    PC = Memory::get_word(PC + 1);
		    SP += 2;

		    cycles += 20;
		} else {
		    PC++;
		    cycles += 8;
		}

		break;
		
	    case 0xD1: // POP DE
		DE = Memory::get_word(SP);
		SP += 2;

		PC++;
		cycles += 12;

		break;
		
	    case 0xD2: // JP NC, A16
		if (!GET_FLAG(C))
		{
		    PC = Memory::get_word(PC + 1);

		    cycles += 16;
		} else {
		    PC += 3;
		    cycles += 12;
		}

		break;
		
	    case 0xD4: // CALL NC, A16
		if (!GET_FLAG(C))
		{
		    SP -=2;
		    PC += 3;
		    Memory::write_word(SP, PC);
		    
		    PC = Memory::get_word(PC + 1);
		    cycles += 24;
		} else {
		    PC += 3;
		    cycles += 12;
		}

		break;
		
	    case 0xD5: // PUSH DE
		SP -= 2;
		Memory::write_word(SP, DE);

		PC++;
		cycles += 16;

		break;
		
	    case 0xD6: // SUB D8
		this->sub_op(Memory::get_byte_ptr(PC + 1));

		PC++;
		cycles += 4;
		
		break;
	    
	    case 0xD7: // RST 10h
		SP -= 2;
		PC++;
		Memory::write_word(SP, PC);

		PC = 0x0010;
		cycles += 16;

		break;
		
	    case 0xD8: // RET C
		if (GET_FLAG(C))
		{
		    PC = Memory::get_word(SP);
		    SP += 2;

		    cycles += 20;
		} else {
		    cycles += 8;
		}
		
		break;
		
	    case 0xD9: // RETI
		intHandler->master_enable(true);
		PC = Memory::get_word(SP);
		SP += 2;

		PC++;
		cycles += 16;

		break;
		
	    case 0xDA: // JP C, A16
		if (GET_FLAG(C))
		{
		    PC = Memory::get_word(PC + 1);
		    cycles += 16;
		} else {
		    cycles += 12;
		}

		break;
		
	    case 0xDC: // CALL C, A16
		if (GET_FLAG(C))
		{
		    SP -= 2;
		    PC += 3;
		    Memory::write_word(SP, PC);

		    cycles += 24;
		} else {
		    PC += 3;
		    cycles += 12;
		}

		break;
		
	    case 0xDE: // SBC D8
		this->sbc_op(Memory::get_byte_ptr(PC + 1));

		PC++;
		cycles += 4;

		break;
		
	    case 0xDF: // RST 18h
		PC++;
		SP -= 2;
		Memory::write_word(SP, PC);

		PC = 0x0018;
		cycles += 16;

		break;
		
	    case 0xE0: // LDH (A8), A
		Memory::write_byte(0xFF00 | Memory::get_byte(PC + 1), A);

		PC += 2;
		cycles += 12;

		break;
		
	    case 0xE1: // POP HL
		HL = Memory::get_word(SP);
		SP += 2;

		PC++;
		cycles += 12;
		
		break;
		
	    case 0xE2: // LD (C), A
		Memory::write_byte(0xFF00 + C, A);

		PC++;
		cycles += 8;

		break;
		
	    case 0xE5: // PUSH HL
		SP -= 2;
		Memory::write_word(SP, HL);
		
		PC++;
		cycles += 16;
		
		break;
		
	    case 0xE6: // AND D8
		this->and_op(Memory::get_byte_ptr(PC + 1));

		PC++;
		cycles += 4;
		
		break;
		
	    case 0xE7: // RST 20h
		PC++;
		SP -= 2;
		Memory::write_word(SP, PC);

		PC = 0x0020;
		cycles += 16;
		
		break;
		
	    case 0xE8: // ADD SP, R8
		SP += (int8_t)Memory::get_byte(PC + 1);

		PC += 2;
		cycles += 16;

		break;
		
	    case 0xE9: // JP (HL)
		PC = HL;
		cycles += 4;
		
		break;
		
	    case 0xEA: // LD (A16), A
		Memory::write_byte(Memory::get_word(PC + 1), A);

		PC += 3;
		cycles += 16;
		
		break;
		
	    case 0xEE: // XOR D8
		this->xor_op(Memory::get_byte_ptr(PC + 1));

		PC++;
		cycles += 4;

		break;
		
	    case 0xEF: // RST 28h
		PC++;
		SP -= 2;
		Memory::write_word(SP, PC);

		PC = 0x0028;
		cycles += 16;
		
		break;
		
	    case 0xF0: // LDH A, (A8)
		A = Memory::get_byte(0xFF00 + Memory::get_byte(PC + 1));

		PC += 2;
		cycles += 12;
		
		break;
		
	    case 0xF1: // POP AF
		// Do not write to the last nibble of the flags register
		AF = Memory::get_word(SP) & 0xFFF0;
		SP += 2;

		PC++;
		cycles += 12;
		
		break;
		
	    case 0xF2: // LD A, (C)
		A = Memory::get_byte(C);

		PC++;
		cycles += 8;
		
		break;
		
	    case 0xF3: // DI
		intHandler->master_enable(false);
		
		break;
		
	    case 0xF5: // PUSH AF
		SP -= 2;
		Memory::write_word(SP, AF);

		PC++;
		cycles += 16;

		break;
		
	    case 0xF6: // OR D8
		this->or_op(Memory::get_byte_ptr(PC + 1));

		PC++;
		cycles += 4;
		
		break;
		
	    case 0xF7: // RST 30h
		SP -= 2;
		PC++;
		Memory::write_word(SP, PC);

		PC = 0x0030;
		cycles += 16;

		break;
		
	    case 0xF8: // LD HL, SP + R8
		if (((SP + Memory::get_byte(PC + 1)) & 0x0F) < (SP & 0x0F)) SET_FLAG(H);
		else CLEAR_FLAG(H);

		if ((word)(SP + Memory::get_byte(PC + 1)) < SP) SET_FLAG(C);
		else CLEAR_FLAG(C);

		CLEAR_FLAG(Z);
		CLEAR_FLAG(N);

		PC += 2;
		cycles += 12;

		break;

	    case  0xF9: // LD SP, HL
		SP = HL;

		PC++;
		cycles += 8;

		break;
		
	    case 0xFA: // LD A, (A16)
		A = Memory::get_byte(Memory::get_word(PC + 1));

		PC += 3;
		cycles += 16;

		break;
		
	    case 0xFB: // EI
		intHandler->master_enable(true);
		
		break;
		
	    case 0xFE: // CP D8
		this->cp_op(Memory::get_byte_ptr(PC + 1));

		PC++;
		cycles += 4;
		
		break;
		
	    case 0xFF: // RST 38h
		SP -= 2;
		PC++;
		Memory::write_word(SP, PC);

		PC = 0x0038;
		cycles += 16;

		break;

	    default:
		printf("Invalid or unimplmented opcode `%#.2x`\n", Memory::get_byte(PC));
		exit(-1);
	}

	Timer::cycle();
    }

    void CPU::interrupt(word vec)
    {
	intHandler->master_enable(false);
	SP -= 2;
	Memory::write_word(SP, PC);
	PC = vec;
    }

    // Decode prefix CB
    void CPU::exec_cb(byte opcode)
    {
	byte* regs[8] =
	{
	    &B, &C, &D, &E, &H, &L, Memory::get_byte_ptr(HL), &A
	};
	
	switch (opcode & 0xF0)
	{
	    case 0x00:
		if ((opcode & 0x0F) < 0x08) this->cb_rlc(regs[opcode & 0x0F]);
		else this->cb_rrc(regs[(opcode & 0x0F) % 8]);

		break;
		
	    case 0x10:
		if ((opcode & 0x0F) < 0x08) this->cb_rl(regs[opcode & 0x0F]);
		else this->cb_rr(regs[(opcode & 0x0F) % 8]);
		
		break;
		
	    case 0x20:
		if ((opcode & 0x0F) < 0x08) this->cb_sla(regs[opcode & 0x0F]);
		else this->cb_sra(regs[(opcode & 0x0F) % 8]);
		
		break;
		
	    case 0x30:
		if ((opcode & 0x0F) < 0x08) this->cb_swap(regs[opcode & 0x0F]);
		else this->cb_srl(regs[(opcode & 0x0F) % 8]);
		
		break;
		
	    case 0x40:
		this->cb_bit(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 0 : 1);
		
		break;
		
	    case 0x50:
		this->cb_bit(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 2 : 3);
		
		break;
		
	    case 0x60:
		this->cb_bit(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 4 : 5);

		break;
		
	    case 0x70:
		this->cb_bit(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) ? 6 : 7);

		break;
		
	    case 0x80:
		this->cb_res(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 0 : 1);

		break;
		
	    case 0x90:
		this->cb_res(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 2 : 3);

		break;
		
	    case 0xA0:
		this->cb_res(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 4 : 5);
		
		break;
		
	    case 0xB0:
		this->cb_res(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 6 : 7);
		
		break;
		
	    case 0xC0:
		this->cb_set(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 0 : 1);
		
		break;
		
	    case 0xD0:
		this->cb_set(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 2 : 3);

		break;
		
	    case 0xE0:
		this->cb_set(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 4 : 5);

		break;
		
	    case 0xF0:
		this->cb_set(regs[(opcode & 0x0F) % 8], (opcode & 0x0F) < 8 ? 6 : 7);

		break;
	}
    }

    void CPU::cb_rlc(byte* op)
    {
	uint32_t flags = _op_rlc(op, GET_FLAG(C));
	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);

	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	CLEAR_FLAG(N);
	CLEAR_FLAG(H);
	
	*op |= GET_FLAG(C);

	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8; // Operations with (HL) as a parameter generally take up 8 cycles
    }

    void CPU::cb_rrc(byte* op)
    {
	uint32_t flags = _op_rrc(op, GET_FLAG(C));
	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);

	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	CLEAR_FLAG(N);
	CLEAR_FLAG(H);
	
	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::cb_rl(byte* op)
    {
	uint32_t flags = _op_rl(op);
	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);

	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	CLEAR_FLAG(N);
	CLEAR_FLAG(H);

	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::cb_rr(byte* op)
    {
	uint32_t flags = _op_rr(op);
	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);

	CLEAR_FLAG(N);
	CLEAR_FLAG(H);

	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::cb_sla(byte* op)
    {
	uint32_t flags = _op_sl(op);
	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);

	CLEAR_FLAG(N);
	CLEAR_FLAG(H);
	
	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::cb_sra(byte* op)
    {
	uint32_t flags = _op_sra(op);
	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	CLEAR_FLAG(N);
	CLEAR_FLAG(H);
	CLEAR_FLAG(C);

	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::cb_swap(byte* op)
    {
	if (*op == 0) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	CLEAR_FLAG(N);
	CLEAR_FLAG(H);
	CLEAR_FLAG(C);

	*op = (*op >> 4) | (*op << 4);

	PC += 2;
 	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::cb_srl(byte* op)
    {
	uint32_t flags = _op_srl(op);

	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);
	
	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::cb_bit(byte* op, byte bit)
    {
	if ((*op & (0x01 << bit)) == 0) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	CLEAR_FLAG(N);
	SET_FLAG(H);

	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::cb_res(byte* op, byte bit)
    {
	*op &= ~(0x01 << bit);

	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::cb_set(byte* op, byte bit)
    {
	*op |= 0x01 << bit;

	PC += 2;
	cycles += op == Memory::get_byte_ptr(HL) ? 16 : 8;
    }

    void CPU::add_op(byte* src)
    {
	uint32_t flags = _op_add(&A, *src);
	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);

	if (GET_OP_FLAG(H, flags)) SET_FLAG(H);
	else CLEAR_FLAG(H);

	CLEAR_FLAG(N);

	PC++;
	cycles += src == Memory::get_byte_ptr(HL) ? 8 : 4;
    }
    
    void CPU::sub_op(byte* src)
    {
	uint32_t flags = _op_sub(&A, *src);
	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);

	if (GET_OP_FLAG(H, flags)) SET_FLAG(H);
	else CLEAR_FLAG(H);

	SET_FLAG(N);
	
	PC++;
	cycles += src == Memory::get_byte_ptr(HL) ? 8 : 4;
    }

    void CPU::adc_op(byte* src)
    {
	uint32_t flags = _op_adc(&A, *src, GET_FLAG(C));
	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	if (GET_OP_FLAG(H, flags)) SET_FLAG(H);
	else CLEAR_FLAG(H);

	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);

	CLEAR_FLAG(N);

	PC++;
	cycles += src == Memory::get_byte_ptr(HL) ? 8 : 4;
    }

    void CPU::sbc_op(byte* src)
    {
	uint32_t flags = _op_sbc(&A, *src, GET_FLAG(C));
	if (GET_OP_FLAG(Z, flags)) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	if (GET_OP_FLAG(H, flags)) SET_FLAG(H);
	else CLEAR_FLAG(H);

	if (GET_OP_FLAG(C, flags)) SET_FLAG(C);
	else CLEAR_FLAG(C);

	SET_FLAG(N);
	
	PC++;
	cycles += src == Memory::get_byte_ptr(HL) ? 8 : 4;
    }

    void CPU::and_op(byte* src)
    {
	if ((A & *src) == 0) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	CLEAR_FLAG(N);
	SET_FLAG(H);
	CLEAR_FLAG(C);
	
	A &= *src;

	PC++;
	cycles += src == Memory::get_byte_ptr(HL) ? 8 : 4;
    }

    void CPU::xor_op(byte* src)
    {
	if ((A ^ *src) == 0) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	CLEAR_FLAG(N);
	CLEAR_FLAG(H);
	CLEAR_FLAG(C);

	A ^= *src;

	PC++;
	cycles += src == Memory::get_byte_ptr(HL) ? 8 : 4;
    }

    void CPU::or_op(byte* src)
    {
	if ((A | *src) == 0) SET_FLAG(Z);
	else CLEAR_FLAG(Z);

	CLEAR_FLAG(N);
	CLEAR_FLAG(H);
	CLEAR_FLAG(C);

	A |= *src;

	PC++;
	cycles += src == Memory::get_byte_ptr(HL) ? 8 : 4;
    }

    void CPU::cp_op(byte* src)
    {
	byte _a = A;
	this->sub_op(src);

	A = _a;

	PC++;
	cycles += src == Memory::get_byte_ptr(HL) ? 8 : 4;
    }
}
