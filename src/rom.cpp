/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>

#include <cassert>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>

#include "rom.hpp"
#include "memory.hpp"

namespace GameboyEmu
{
    namespace ROM
    {
	byte* rom;
	int romsize = 0;
	MBC mbc_controller;
	
	const byte nintendo_logo[] =
	{
	    0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
	    0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
	    0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E
	};

	const char* cartridge_types[] =
	{
	    [0x00] = "ROM ONLY",
	    [0x01] = "MBC1",
	    [0x02] = "MBC1 + RAM",
	    [0x03] = "MBC1 + RAM + BATTERY",
	    [0x05] = "MBC2",
	    [0x06] = "MBC2 + BATTERY",
	    [0x08] = "ROM + RAM",
	    [0x09] = "ROM + RAM + BATTERY",
	    [0x0B] = "MMM01",
	    [0x0C] = "MMM01 + RAM",
	    [0x0D] = "MMM01 + RAM + BATTERY",
	    [0x0F] = "MBC3 + TIMER + BATTERY",
	    [0x10] = "MBC3 + TIMER + RAM + BATTERY",
	    [0x11] = "MBC3",
	    [0x12] = "MBC3 + RAM",
	    [0x13] = "MBC3 + RAM + BATTERY",
	    [0x15] = "MBC4",
	    [0x16] = "MBC4 + RAM",
	    [0x17] = "MBC4 + RAM + BATTERY",
	    [0x19] = "MBC5",
	    [0x1A] = "MBC5 + RAM",
	    [0x1B] = "MBC5 + RAM + BATTERY",
	    [0x1C] = "MBC5 + RUMBLE",
	    [0x1D] = "MBC5 + RUMBLE + RAM",
	    [0x1E] = "MBC5 + RUMBLE + RAM + BATTERY",
	    [0xFC] = "POCKET CAMERA",
	    [0xFD] = "BANDAI TAMA5",
	    [0xFE] = "HuC3",
	    [0xFF] = "HuC1 + RAM + BATTERY"
	};

	const char* rom_sizes[] =
	{
	    [0x00] = "32KB (No Banking)",
	    [0x01] = "64KB (4 Banks)",
	    [0x02] = "128KB (8 Banks)",
	    [0x03] = "256KB (16 Banks)",
	    [0x04] = "512KB (32 Banks)",
	    [0x05] = "1MB (64 Banks)",
	    [0x06] = "2MB (128 Banks)",
	    [0x07] = "4MB (256 Banks)",
	    [0x52] = "1.1MB (72 Banks)",
	    [0x53] = "1.2MB (80 Banks)",
	    [0x54] = "1.5MB (96 Banks)",
	};

	const char* ram_sizes[] =
	{
	    [0x00] = "None",
	    [0x01] = "2KB",
	    [0x02] = "4KB",
	    [0x03] = "32KB",
	};

	const char* regions[] =
	{
	    [0x00] = "Japanese",
	    [0x01] = "Non-Japanese"
	};

	bool load_rom(const char* fname)
	{
	    std::ifstream file(fname);

	    if (!file.is_open())
	    {
		printf("Unable to open file `%s`\n", fname);
		return false;
	    }

	    // Attempt to get the size of the ROM, else fail
	    struct stat st;
	    unsigned filesize = stat(fname, &st) == 0 ? st.st_size : 0;

	    if (filesize == 0)
	    {
		printf("Unable to read ROM, or ROM is empty\n");
		return false;
	    }

	    // Make rom big enough to hold the entire contents of the file
	    rom = new byte[filesize];
	    romsize = filesize;

	    for (int i = 0; !file.eof(); i++)
	    {
		char c;
		file.read(&c, 1);
		rom[i] = c;
	    }

	    // Check if the first 48 bytes of rom are exactly equal to the nintendo logo, else fail
	    if (memcmp(nintendo_logo, rom + 0x0104, sizeof(nintendo_logo)) != 0)
	    {
		printf("Invalid or corrupt ROM `%s`\n", fname);
		return false;
	    }

	    char title[16];
	    char man_code[4];
	    char new_lic_code[2];
	    char* mbc;
	    char* romsize;
	    char* ramsize;
	    char* region;
	    byte old_lic_code;
	    byte rom_version;
	    byte checksum = 0;
	    bool checksum_pass;
	    
	    strncpy(title,        (char*)rom + 0x0134, 16);
	    strncpy(man_code,     (char*)rom + 0x013F,  4);
	    strncpy(new_lic_code, (char*)rom + 0x0144,  2);
	    
	    mbc          = (char*)cartridge_types[rom[0x0147]];
	    romsize      = (char*)rom_sizes[rom[0x0148]];
	    ramsize      = (char*)ram_sizes[rom[0x0149]];
	    region       = (char*)regions[rom[0x014A]];
	    old_lic_code = rom[0x014B];
	    rom_version  = rom[0x014C];

	    for (word i = 0x0134; i <= 0x014C; i++)
		checksum = checksum - rom[i] - 1;

	    checksum_pass = checksum == rom[0x014D];

	    printf("ROM Title:            %s\n",    title);
	    printf("Manufacturer:         %s\n",    man_code);
	    printf("License Code (New):   %s\n",    new_lic_code);
	    printf("MBC Type:             %s\n",    mbc);
	    printf("ROM Size:             %s\n",    romsize);
	    printf("RAM Size:             %s\n",    ramsize);
	    printf("Region:               %s\n",    region);
	    printf("License Code (Old):   %x %s\n", old_lic_code, old_lic_code == 0x33 ? "(Use New Code)" : "");
	    printf("ROM Version:          %x\n",    rom_version);
	    printf("Checksum:             %s\n",    checksum_pass ? "PASS" : "FAIL");

	    if (!checksum_pass)
	    {
		printf("\nInvalid Checksum. Exiting...\n");
		return false;
	    }

	    // Set the MBC type
	    switch (rom[0x0147])
	    {
		case 0x00: mbc_controller.set_mbc(MBC_Type::NONE); break;
		case 0x01: mbc_controller.set_mbc(MBC_Type::MBC1); break;
		case 0x02:
		    mbc_controller.set_mbc(MBC_Type::MBC1);
		    mbc_controller.set_mbc_extra(MBC_Extra::RAM);
		    break;

		case 0x03:
		    mbc_controller.set_mbc(MBC_Type::MBC1);
		    mbc_controller.set_mbc_extra(MBC_Extra::RAM | MBC_Extra::BATTERY);
		    break;

		case 0x05: mbc_controller.set_mbc(MBC_Type::MBC2); break;
		case 0x06: 
		    mbc_controller.set_mbc(MBC_Type::MBC2);
		    mbc_controller.set_mbc_extra(MBC_Extra::BATTERY);
		    break;
		    
		case 0x08:
		    mbc_controller.set_mbc(MBC_Type::NONE);
		    mbc_controller.set_mbc_extra(MBC_Extra::RAM);
		    break;

		case 0x09:
		    mbc_controller.set_mbc(MBC_Type::NONE);
		    mbc_controller.set_mbc_extra(MBC_Extra::RAM | MBC_Extra::BATTERY);
		    break;

		case 0x0F:
		    mbc_controller.set_mbc(MBC_Type::MBC3);
		    mbc_controller.set_mbc_extra(MBC_Extra::TIMER | MBC_Extra::BATTERY);
		    break;

		case 0x10:
		    mbc_controller.set_mbc(MBC_Type::MBC3);
		    mbc_controller.set_mbc_extra(MBC_Extra::TIMER | MBC_Extra::RAM | MBC_Extra::BATTERY);
		    break;

		case 0x11: mbc_controller.set_mbc(MBC_Type::MBC3); break;
		case 0x12:
		    mbc_controller.set_mbc(MBC_Type::MBC3);
		    mbc_controller.set_mbc_extra(MBC_Extra::RAM);
		    break;
		    
		case 0x13:
		    mbc_controller.set_mbc(MBC_Type::MBC3);
		    mbc_controller.set_mbc_extra(MBC_Extra::RAM | MBC_Extra::BATTERY);
		    break;

		default:
		    printf("Unsupported MBC `%s`. Exiting...\n", mbc);
		    return false;
	    }
	    
	    return true;
	}

	byte* get_rom()
	{
	    return rom;
	}

	int rom_size()
	{
	    return romsize;
	}

	MBC* get_mbc_controller()
	{
	    return &mbc_controller;
	}

	void destroy()
	{
	    delete[] rom;
	    rom = nullptr;
	}
    }
}
