/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstring>

#include "mbc.hpp"
#include "memory.hpp"

namespace GameboyEmu
{
    MBC_None::MBC_None() {  }
    MBC_None::~MBC_None() {  }

    /*
     *  No MBC
     *  32KB ROM
     */

    bool MBC_None::write_byte(word address, byte value)
    {
	return address < 0x8000;
    }

    
    /*
     *  MBC1
     *  Max 2MB ROM, Optional 32KB RAM
     *
     */

    MBC1::MBC1() { ram_banks = new byte[0x8000]; }

    bool MBC1::write_byte(word address, byte value)
    {
	if (address < 0x2000) // External RAM enable
	{
	    RAM_enable = (value & 0x0F) == 0x0A;
	} else if (address >= 0x2000 && address < 0x4000) { // ROM bank number select
	    ROM_bank = value & 0x0F;
	    ROM_bank |= RAM_banking_mode ? 0 : upper_bank_bits;

 	    if (ROM_bank == 0x20 || ROM_bank == 0x40 || ROM_bank == 0x60) ROM_bank++;
	    
	    Memory::switch_rom_bank(ROM_bank, MBC_Type::MBC1);
	} else if (address >= 0x4000 && address < 0x6000) { // RAM Bank select || ROM bank number upper bits
	    if (RAM_banking_mode)
	    {
		// Copy the currently selected RAM bank back into ram_banks
		memcpy(ram_banks + (0x2000 * RAM_bank), Memory::get_memory() + 0xA000, 0x2000);

		byte* ram = new byte[0x2000];
		RAM_bank = value & 0x03;

		// Copy the new RAM bank into memory
		memcpy(ram, ram_banks + (0x2000 * RAM_bank), 0x2000);
		Memory::switch_ram_bank(ram);
		delete[] ram;
	    } else upper_bank_bits = (value & 0x03) << 5;
	} else if (address >= 0x6000 && address < 0x8000) { // ROM/RAM mode set
	    RAM_banking_mode = value == 0x01;
	} else {
	    return false;
	}

	return true;
    }


    /*
     *  MBC2
     *  Max 256KB ROM, 512 x 4 bits RAM
     *
     */

    MBC2::MBC2() { ram_banks = new byte[0x0200]; }
    
    bool MBC2::write_byte(word address, byte value)
    {
	if (address < 0x2000 && (address & 0x0100) == 0) // RAM enable (only set if the LSB of the upper address byte is 0)
	{
	    if (value == 0) RAM_enable = false;
	    else if (value == 0x0A) RAM_enable = true;
	} else if (address >= 0x2000 && address < 0x4000 && (address & 0x0100) == 1) { // ROM bank number
	    ROM_bank = value & 0x0F;
	    if (ROM_bank == 0) ROM_bank = 1;

	    Memory::switch_rom_bank(ROM_bank, MBC_Type::MBC2);
	} else {
	    return false;
	}

	return true;
    }


    /*
     *  MBC3
     *  Max 2MB ROM, Optional 32KB RAM, Optional RTC (to be implemented)
     *
     */

    MBC3::MBC3() { ram_banks = new byte[0x8000]; }
    
    bool MBC3::write_byte(word address, byte value)
    {
	if (address < 0x2000) // RAM/RTC enable
	{
	    RAM_enable = value == 0x0A;
	} else if (address >= 0x2000 && address < 0x4000) { // ROM bank number
	    ROM_bank = value & 0x7F;

	    Memory::switch_rom_bank(ROM_bank, MBC_Type::MBC3);
	} else if (address >= 0x4000 && address < 0x6000) { // RAM bank number || RTC register select
	    // TODO: RTC register select
	    
	    if (RAM_enable)
	    {
		memcpy(ram_banks + (0x2000 * RAM_bank), Memory::get_memory() + 0xA000, 0x2000);
	    
		byte* ram = new byte[0x2000];
		RAM_bank = value & 0x03;
	    
		memcpy(ram, ram_banks + (0x2000 * RAM_bank), 0x2000);
		Memory::switch_ram_bank(ram);
		delete[] ram;
	    }

	} else if (address >= 0x6000 && address < 0x8000) { // Latch clock data
	    // TODO: RTC stuff && things
	} else {
	    return false;
	}

	return true;
    }


    /*
     *  MBC5
     *  8MB ROM, optional 128KB RAM
     *
     */

    MBC5::MBC5() {  }
    bool MBC5::write_byte(word address, byte value)
    {
	if (address < 0x2000)
	{
	    RAM_enable = value == 0x0A;
	} else if (address >= 0x2000 && address < 0x3000) {
	    lower_bank_bits = value;
	} else if (address >= 0x3000 && address < 0x4000) {
	    upper_bank_bits = value & 0x01;
	    word bank = (upper_bank_bits << 8) | lower_bank_bits;
	    Memory::switch_rom_bank(bank, MBC_Type::MBC5);
	} else if (address >= 0x4000 && address < 0x6000) {
	    if (RAM_enable)
	    {
		memcpy(ram_banks + (0x2000 * RAM_bank), Memory::get_memory() + 0xA000, 0x2000);
	    
		byte* ram = new byte[0x2000];
		RAM_bank = value & 0x03;
	    
		memcpy(ram, ram_banks + (0x2000 * RAM_bank), 0x2000);
		Memory::switch_ram_bank(ram);
		delete[] ram;
	    }
	} else {
	    return false;
	}
	
	return true;
    }


    // MBC Controller class
    void MBC::set_mbc(MBC_Type t)
    {
	switch (t)
	{
	    case MBC_Type::NONE: current_mbc = &none; break;
	    case MBC_Type::MBC1: current_mbc = &mbc1; break;
	    case MBC_Type::MBC2: current_mbc = &mbc2; break;
	    case MBC_Type::MBC3: current_mbc = &mbc3; break;
	    case MBC_Type::MBC5: current_mbc = &mbc5; break;
	}
    }

    MBC_Type MBC::get_mbc()
    {
	if (current_mbc == &mbc1)      return MBC_Type::MBC1;
	else if (current_mbc == &mbc2) return MBC_Type::MBC2;
	else if (current_mbc == &mbc3) return MBC_Type::MBC3;
	else if (current_mbc == &mbc5) return MBC_Type::MBC5;

	return MBC_Type::NONE;
    }

    void MBC::set_mbc_extra(MBC_Extra e)
    {
	extra = e;
    }

    MBC_Extra MBC::get_mbc_extra()
    {
	return extra;
    }

    void MBC::write_ram(word address, byte value)
    {
	word offset = address - 0xA000;
	current_mbc->ram_banks[offset * current_mbc->RAM_bank] = value;
    }

    byte MBC::read_ram(word address)
    {
	word offset = address - 0xA000;
	return current_mbc->ram_banks[offset * current_mbc->RAM_bank];
    }
}
