/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEMORY_H
#define MEMORY_H

#include <array>

#include "mbc.hpp"
#include "types.hpp"

namespace GameboyEmu
{
    namespace Memory
    {
	void init();
	void destroy();
	void write_byte(word address, byte value);
	void write_word(word address, word value);
	byte get_byte  (word address);
	byte* get_byte_ptr(word address);
	word get_word  (word address);
	byte* get_memory();
	void switch_rom_bank(word bank, MBC_Type type);
	void switch_ram_bank(byte* ram);

	// Interrupt registers
	extern byte* IE;
	extern byte* IF;

	// Timer registers
	extern byte* DIV;
	extern byte* TIMA;
	extern byte* TMA;
	extern byte* TAC;

	// LCD Registers
	extern byte* LCDC;
	extern byte* STAT;
	extern byte* SCY;
	extern byte* SCX;
	extern byte* LY;
	extern byte* LYC;
	extern byte* WY;
	extern byte* WX;
    }
}

#endif
