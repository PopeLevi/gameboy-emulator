/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MBC_H
#define MBC_H

#include "types.hpp"

#define MBC_CLASS_EX(X, Y)					\
class MBC##X : public MBC_Base					\
{								\
    public:							\
    	MBC##X();						\
	bool write_byte(word address, byte value) override;	\
	Y							\
}

#define MBC_CLASS(X) MBC_CLASS_EX(X,)

namespace GameboyEmu
{
    enum class MBC_Type : byte
    {
	NONE,
	MBC1,
	MBC2,
	MBC3,
	MBC5
    };

    enum class MBC_Extra : byte
    {
	NONE,
	RAM,
	BATTERY,
	TIMER
    };

    inline MBC_Extra operator| (MBC_Extra a, MBC_Extra b)
    {
	return static_cast<MBC_Extra>(static_cast<byte>(a) | static_cast<byte>(b));
    }

    class MBC_Base
    {
	public:
	    ~MBC_Base() { delete[] ram_banks; ram_banks = nullptr; }
	    virtual bool write_byte(word address, byte value) = 0;

	protected:
	    friend class MBC;

	    byte ROM_bank = 1;
	    byte RAM_bank = 1;
	    bool RAM_enable = false;
	    bool RAM_banking_mode = false;
	    byte upper_bank_bits = 0;
	    byte* ram_banks;
    };

    // OMGZ Super amazing preprocessor hacking
    MBC_CLASS_EX(_None, ~MBC_None(););
    MBC_CLASS(1);
    MBC_CLASS(2);
    MBC_CLASS(3);
    MBC_CLASS_EX(5, byte lower_bank_bits;);
    
    class MBC
    {
	public:
	    void set_mbc(MBC_Type t);
	    MBC_Type get_mbc();
	    void set_mbc_extra(MBC_Extra e);
	    MBC_Extra get_mbc_extra();
	    void write_ram(word address, byte value);
	    byte read_ram(word address);
	    byte* get_ram();

	    MBC_Base* current_mbc = &none;

	private:

	    MBC_None none;
	    MBC1 mbc1;
	    MBC2 mbc2;
	    MBC3 mbc3;
	    MBC5 mbc5;
	    MBC_Extra extra = MBC_Extra::NONE;
    };
}

#endif
