/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPU_H
#define CPU_H

#include "types.hpp"

#define SET_FLAG(X) SET_##X##_FLAG()

#define SET_Z_FLAG() F |= 0x80 // Zero
#define SET_N_FLAG() F |= 0x40 // Negative
#define SET_H_FLAG() F |= 0x20 // Half-carry
#define SET_C_FLAG() F |= 0x10 // Carry

#define CLEAR_FLAG(X) CLEAR_##X##_FLAG()

#define CLEAR_Z_FLAG() F &= ~0x80
#define CLEAR_N_FLAG() F &= ~0x40
#define CLEAR_H_FLAG() F &= ~0x20
#define CLEAR_C_FLAG() F &= ~0x10

#define GET_FLAG(X) GET_##X##_FLAG()

#define GET_Z_FLAG() !!(F & 0x80)
#define GET_N_FLAG() !!(F & 0x40)
#define GET_H_FLAG() !!(F & 0x20)
#define GET_C_FLAG() !!(F & 0x10)

namespace GameboyEmu
{
    class InterruptHandler;

    class CPU
    {
	public:
	    CPU();
	    ~CPU();
	    void run_cycle();
	    word get_pc();

	private:

	    // Main registers
	    struct
	    {
		union
		{
		    word AF;
		    struct { byte F; byte A; };
		};

		union
		{
		    word BC;
		    struct { byte C; byte B; };
		};

		union
		{
		    word DE;
		    struct { byte E; byte D; };
		};

		union
		{
		    word HL;
		    struct { byte L; byte H; };
		};

	    };

	    word SP; // Stack pointer
	    word PC; // Program counter

	    uint64_t cycles;
	    bool halt;

	    friend class InterruptHandler;
	    InterruptHandler* intHandler;

	    void interrupt(word vec);
	    
	    // Prefix CB
	    void exec_cb(byte opcode);
	    void cb_rlc (byte* op);
	    void cb_rrc (byte* op);
	    void cb_rl  (byte* op);
	    void cb_rr  (byte* op);
	    void cb_sla (byte* op);
	    void cb_sra (byte* op);
	    void cb_swap(byte* op);
	    void cb_srl (byte* op);
	    void cb_bit (byte* op, byte bit);
	    void cb_res (byte* op, byte bit);
	    void cb_set (byte* op, byte bit);

	    // Random assortment of arithmetic and bitwise operations
	    void add_op(byte* src);
	    void sub_op(byte* src);
	    void adc_op(byte* src);
	    void sbc_op(byte* src);
	    void and_op(byte* src);
	    void xor_op(byte* src);
	    void or_op (byte* src);
	    void cp_op (byte* src);
    };
}

#endif
