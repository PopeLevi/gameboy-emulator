;; Copyright (C) 2015  Alister Sanders

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;; Rotate/Shift instructions
global _op_rr		;; Rotate right
global _op_rl		;; Rotate left
global _op_rrc		;; Rotate right with carry
global _op_rlc		;; Rotate left with carry
global _op_sra		;; Shift right arithmetic
global _op_srl		;; Shift right logical
global _op_sl		;; Shift left 

;; Arithmetic instructions
global _op_add		;; Add (8 bit)
global _op_sub		;; Subtract (8 bit)
global _op_adc		;; Add with carry (8 bit)
global _op_sbc		;; Subtract with carry (8 bit)
global _op_add16	;; Add (16 bit)

section .text
	get_flags:
		pushf			;; Push the EFLAGS register onto the stack
		pop	rax		;; Set the return value to the value of EFLAGS
		ret
	
	set_carry:
		xor	r8, r8
		cmp	r9b, 0		;; If r9b == 0, the emulator's carry flag has not been set
		setnz	r8b		;; Set r8b to 1 if the emulator's carry flag is set
		call	get_flags
		or	rax, r8
		push	rax
		popf			;; Make the appropriate changes to EFLAGS
		ret

	;; uint32_t _op_rr(byte* reg)
	_op_rr:
		ror	byte [ rdi ], 1
		call 	get_flags
		ret

	;; uint32_t _op_rl(byte* reg)
	_op_rl:
		rol	byte [ rdi ], 1
		call 	get_flags
		ret

	;; uint32_t _op_rrc(byte* reg, byte carry)
	_op_rrc:
		mov	r9, rsi		;; Second argument, the value of the emulator's carry flag
		call	set_carry	;; Manually set the carry flag
		rcr	byte [ rdi ], 1
		call 	get_flags
		ret

	;; uint32_t _op_rlc(byte* reg, byte carry)
	_op_rlc:
		mov	r9, rsi
		call	set_carry
		rcl	byte [ rdi ], 1
		call 	get_flags
		ret

	;; uint32_t _op_sra(byte* reg)
	_op_sra:
		sar	byte [ rdi ], 1
		call	get_flags
		ret

	;; uint32_t _op_srl(byte* reg)
	_op_srl:
		shr	byte [ rdi ], 1
		call	get_flags
		ret

	;; uint32_t _op_sl(byte* reg)
	_op_sl:
		shl	byte [ rdi ], 1
		call	get_flags
		ret

	;; uint32_t _op_add(byte* reg, byte op)
	_op_add:
		mov	r10, rsi
		add	byte [ rdi ], r10b
		call	get_flags
		ret
	
	;; uint32_t _op_sub(byte* reg, byte op)
	_op_sub:
		mov	r10, rsi
		sub	byte [ rdi ], r10b
		call	get_flags
		ret

	;; uint32_t _op_adc(byte* reg, byte op, byte carry)
	_op_adc:
		mov	r9, rdx
		mov	r10, rsi		;; Second addition operand
		call	set_carry
		adc	byte [ rdi ], r10b
		call	get_flags
		ret

	;; uint32_t _op_sbc(byte* reg, byte op, byte carry)
	_op_sbc:
		mov	r9, rdx
		mov	r10, rsi
		call	set_carry
		sbb	byte [ rdi ], r10b
		call	get_flags
		ret

	;; uint32_t _op_add16(word* reg, word op)
	_op_add16:
		mov	r10, rsi
		add	word [ rdi ], r10w
		call	get_flags
		ret
