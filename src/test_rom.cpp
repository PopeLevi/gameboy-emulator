#include <cassert>

#include "rom.hpp"

int main(int argc, char *argv[])
{
    assert(argc == 2);

    GameboyEmu::ROM::load_rom(argv[1]);
    
    return 0;
}
