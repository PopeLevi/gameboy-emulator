#include <cstdio>
#include <cassert>

#include <unistd.h>

#include "cpu.hpp"
#include "memory.hpp"
#include "types.hpp"

int main(int argc, char* argv[])
{
    assert(argc == 2);

    GameboyEmu::CPU cpu;
    
    FILE* input = nullptr;
    if ((input = fopen(argv[1], "r")) == nullptr)
    {
	printf("Unable to open `%s` for reading\n", argv[1]);
	return 1;
    }

    int loops = 0x100;
    int i = 0;
    
    while ((i = fgetc(input)) != EOF)
    {
	GameboyEmu::Memory::write_byte(loops++, i);
	printf("%.2X ", i);
	if (loops % 16 == 0 && loops != 0x100) putchar('\n');
    }

    while (GameboyEmu::Memory::get_byte(cpu.get_pc()))
	cpu.run_cycle();
    
    putchar('\n');
    
    return 0;
}
