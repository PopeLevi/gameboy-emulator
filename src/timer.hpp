#ifndef TIMER_H
#define TIMER_H

#include "interrupt.hpp"
#include "memory.hpp"
#include "types.hpp"

namespace GameboyEmu
{
    namespace Timer
    {
	void init(InterruptHandler* inth);
	void cycle();
	void set_divider();
	void set_counter(byte value);
	void set_modulo(byte value);
	void set_control(byte value);
    };
}

#endif
