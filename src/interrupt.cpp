/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "interrupt.hpp"
#include "cpu.hpp"

namespace GameboyEmu
{
    InterruptHandler::InterruptHandler()
    {
	master_flag = true;
	IE = Memory::IE;
	IF = Memory::IF;
    }

    void InterruptHandler::master_enable(bool b)
    {
	master_flag = b;
    }

    void InterruptHandler::enable_int(Interrupt i, bool enable)
    {
	if (enable) *IE |= (byte)i;
	else *IE &= ~(byte)i;
    }

    bool InterruptHandler::int_enabled(Interrupt i)
    {
	return (*IE) & i;
    }
    
    void InterruptHandler::process_int(Interrupt i)
    {
	*IF |= i;

	if ((*IF & V_BLANK) == V_BLANK)
	{
	    cpu->interrupt(0x0040);
	    *IF &= ~V_BLANK;
	} else if ((*IF & LCD_STAT) == LCD_STAT) {
	    cpu->interrupt(0x0048);
	    *IF &= ~LCD_STAT;
	} else if ((*IF & TIMER) == TIMER) {
	    cpu->interrupt(0x0050);
	    *IF &= ~TIMER;
	} else if ((*IF & SERIAL) == SERIAL) {
	    cpu->interrupt(0x0058);
	    *IF &= ~SERIAL;
	} else if ((*IF & JOYPAD) == JOYPAD) {
	    cpu->interrupt(0x0060);
	    *IF &= ~JOYPAD;
	}
    }

    void InterruptHandler::set_cpu(CPU* c)
    {
	cpu = c;
    }
}
