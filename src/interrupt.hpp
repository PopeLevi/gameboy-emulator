/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTERRUPT_H
#define INTERRUPT_H

#include "memory.hpp"
#include "types.hpp"

enum Interrupt : byte
{
    V_BLANK   = 0x01,
    LCD_STAT  = 0x02,
    TIMER     = 0x04,
    SERIAL    = 0x08,
    JOYPAD    = 0x10
};

namespace GameboyEmu
{
    class CPU;

    class InterruptHandler
    {
	public:
	    InterruptHandler();

	    void master_enable(bool b);
	    void enable_int(Interrupt i, bool enable);
	    bool int_enabled(Interrupt i);
	    void process_int(Interrupt i);
	    void set_cpu(CPU* c);

	private:
	    bool master_flag;

	    byte* IE; // Interrupt enable
	    byte* IF; // Interrupt flag

	    CPU* cpu;
    };
}

#endif
