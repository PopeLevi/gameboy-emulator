/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstring>

#include "memory.hpp"
#include "rom.hpp"
#include "timer.hpp"
#include "types.hpp"

namespace GameboyEmu
{
    namespace Memory
    {
	byte* memory = nullptr;
	byte* rom    = nullptr;

	byte* IF   = nullptr;
	byte* IE   = nullptr;
	byte* DIV  = nullptr;
	byte* TIMA = nullptr;
	byte* TMA  = nullptr;
	byte* TAC  = nullptr;

	byte* LCDC = nullptr;
	byte* STAT = nullptr;
	byte* SCY  = nullptr;
	byte* SCX  = nullptr;
	byte* LY   = nullptr;
	byte* LYC  = nullptr;
	byte* WY   = nullptr;
	byte* WX   = nullptr;

	MBC* mbc_controller;
	
	void init()
	{
	    memory = new byte[0x10000];
	    memset(memory, 0, 0x10000);
	    
	    // Hardware IO registers
	    memory[0xFF05] = 0x00; // TIMA
	    memory[0xFF06] = 0x00; // TMA
	    memory[0xFF07] = 0x00; // TAC
	    memory[0xFF10] = 0x80; // NR10
	    memory[0xFF11] = 0xBF; // NR11
	    memory[0xFF12] = 0xF3; // NR12
	    memory[0xFF14] = 0xBF; // NR14
	    memory[0xFF16] = 0x3F; // NR21
	    memory[0xFF17] = 0x00; // NR22
	    memory[0xFF19] = 0xBF; // NR24
	    memory[0xFF1A] = 0x7F; // NR30
	    memory[0xFF1B] = 0xFF; // NR31
	    memory[0xFF1C] = 0x9F; // NR32
	    memory[0xFF1E] = 0xBF; // NR33
	    memory[0xFF20] = 0xFF; // NR41
	    memory[0xFF21] = 0x00; // NR42
	    memory[0xFF22] = 0x00; // NR43
	    memory[0xFF23] = 0xBF; // NR30
	    memory[0xFF24] = 0x77; // NR50
	    memory[0xFF25] = 0xF3; // NR51
	    memory[0xFF26] = 0xF1; // NR52
	    memory[0xFF40] = 0x91; // LCDC
	    memory[0xFF42] = 0x00; // SCY
	    memory[0xFF43] = 0x00; // SCX
	    memory[0xFF45] = 0x00; // LYC
	    memory[0xFF47] = 0xFC; // BGP
	    memory[0xFF48] = 0xFF; // OBP0
	    memory[0xFF49] = 0xFF; // OBP1
	    memory[0xFF4A] = 0x00; // WY
	    memory[0xFF4B] = 0x00; // WX
	    memory[0xFFFF] = 0x00; // IE

	    // Interrupt registers
	    IE = &(memory[0xFFFF]);
	    IF = &(memory[0xFF0F]);

	    // Timer registers
	    DIV  = &(memory[0xFF04]);
	    TIMA = &(memory[0xFF05]);
	    TMA  = &(memory[0xFF06]);
	    TAC  = &(memory[0xFF07]);

	    LCDC = &(memory[0xFF40]);
	    STAT = &(memory[0xFF41]);
	    SCY  = &(memory[0xFF42]);
	    SCX  = &(memory[0xFF43]);
	    LY   = &(memory[0xFF44]);
	    LYC  = &(memory[0xFF45]);
	    WY   = &(memory[0xFF4A]);
	    WX   = &(memory[0xFF4B]);

	    mbc_controller = ROM::get_mbc_controller();
	}

	void destroy()
	{
	    delete[] memory;
	}

	void write_byte(word address, byte value)
	{
	    if (mbc_controller->current_mbc->write_byte(address, value)) return;

	    switch (address)
	    {
		case 0xFF04: memory[address] = 0; break;
		case 0xFF07: Timer::set_control(value); // Intentional fall-through

		default: memory[address] = value;
	    }
	}

	void write_word(word address, word value)
	{
	    memory[address] = value & 0x00FF;
	    memory[address + 1] = value >> 8;
	}

	byte get_byte(word address)
	{
	    return memory[address];
	}

	byte* get_byte_ptr(word address)
	{
	    return &(memory[address]);
	}

	word get_word(word address)
	{
	    return (memory[address + 1] << 8) | memory[address];
	}

	byte* get_memory()
	{
	    return memory;
	}

	void switch_rom_bank(byte bank, MBC_Type type)
	{
	    switch_rom_bank((word)bank, type);
	}
	
	void switch_rom_bank(word bank, MBC_Type type)
	{
	    if (!bank && type != MBC_Type::MBC5) bank = 1; // Bank cannot have a value of 0 unless we are dealing with MBC5

	    // Copy a block of memory 16KB long from the ROM into memory
	    memcpy(memory + 0x4000, &ROM::get_rom()[bank * 0x4000], 0x4000);
	}

	void switch_ram_bank(byte* ram)
	{
	    memcpy(memory + 0xA000, ram, 0x2000);
	}
    }
}
