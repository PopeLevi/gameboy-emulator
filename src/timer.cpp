#include "timer.hpp"

namespace GameboyEmu
{
    namespace Timer
    {
	bool enable;
	    
	byte* div;
	byte* tima;
	byte* tma;
	byte* tac;
	
	uint32_t speed;
	uint32_t timer_speeds[4] =
	{
	    4096, 262144, 65536, 16384
	};

	InterruptHandler* intHandler;

	extern "C" // Use C code to gain access to timespec. For some reason this throws errors in C++
	{
	    #include <time.h>

	    struct timespec tm;
	    unsigned long elapsed = [](struct timespec* tm) -> unsigned long
	    {
		// Grab the current CPU time
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, tm);
		return tm->tv_nsec;
	    } (&tm);
    
	    unsigned long timing_get_diff()
	    {
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &tm);
		elapsed = tm.tv_nsec - elapsed;
		return elapsed;
	    }
	}

	void init(InterruptHandler* inth)
	{
	    enable = true;
	    speed = timer_speeds[0];

	    div  = Memory::DIV;
	    tima = Memory::TIMA;
	    tma  = Memory::TMA;
	    tac  = Memory::TAC;

	    intHandler = inth;
	}

	void cycle()
	{
	    unsigned long diff = timing_get_diff();
	    unsigned long long time = 1000000000 / 16384; // For DIV register
	    if (diff >= time)
		(*div)++;

	    if (!enable) return;
	
	    time = 1000000000 / speed; // Now we process TIMA
	    if (diff >= time)
	    {
		(*tima)++;

		// If the timer has overflown, load TMA into TIMA and interrupt
		if (*tima == 0)
		{
		    *tima = *tma;
		    if (intHandler->int_enabled(TIMER)) intHandler->process_int(TIMER);
		}
	    }
	}

	void set_control(byte value)
	{
	    enable = value & 0x04;
	    speed = timer_speeds[value & 0x03];
	}
    }
}
