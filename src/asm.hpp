/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASM_H
#define ASM_H

#include "types.hpp"

/*
 *  x86 EFLAGS register
 *  Bit 0: Carry flag
 *  Bit 4: Adjust (Half carry) flag
 *  Bit 6: Zero flag
 *
 */

#define GET_OP_FLAG_Z(X)  ( X & 0x40 )
#define GET_OP_FLAG_H(X)  ( X & 0x10 )
#define GET_OP_FLAG_C(X)  ( X & 0x01 )
#define GET_OP_FLAG(X, Y) GET_OP_FLAG_##X(Y)

// Functions implemented inside operations.s
// All of these functions return the status of the machine's EFLAGS register
extern "C" uint32_t _op_rr   (byte* reg);
extern "C" uint32_t _op_rl   (byte* reg);
extern "C" uint32_t _op_rrc  (byte* reg, byte carry);
extern "C" uint32_t _op_rlc  (byte* reg, byte carry);
extern "C" uint32_t _op_sra  (byte* reg);
extern "C" uint32_t _op_srl  (byte* reg);
extern "C" uint32_t _op_sl   (byte* reg);
extern "C" uint32_t _op_add  (byte* reg, byte op);
extern "C" uint32_t _op_sub  (byte* reg, byte op);
extern "C" uint32_t _op_adc  (byte* reg, byte op, byte carry);
extern "C" uint32_t _op_sbc  (byte* reg, byte op, byte carry);
extern "C" uint32_t _op_add16(word* reg, word op);

#endif
