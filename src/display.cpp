/*
 * Copyright (C) 2015  Alister Sanders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "display.hpp"
#include "memory.hpp"

namespace GameboyEmu
{
    namespace LCD
    {
	byte* lcdc; // LCD Control
	byte* stat; // LCD status register
	byte* scy;  // Scroll Y
	byte* scx;  // Scroll X
	byte* ly;   // LCDC Y Coordinate
	byte* lyc;  // LY Compare
	byte* wy;   // Window Y
	byte* wx;   // Window X

	const dword pallete[4] =
	{
	    0xFFFFFF,	// White
	    0xC0C0C0,   // Light grey
	    0x808080,   // Dark grey
	    0x000000    // Black
	};

	byte background_pallete[4] =
	{
	    0, 1, 2, 3
	};
	
	byte sprite_pallete1[4] =
	{
	    0, 1, 2, 3
	};

	byte sprite_pallete2[4] =
	{
	    0, 1, 2, 3
	};

	void init()
	{
	    lcdc = Memory::LCDC;
	    stat = Memory::STAT;
	    scy  = Memory::SCY;
	    scx  = Memory::SCX;
	    ly   = Memory::LY;
	    lyc  = Memory::LYC;
	    wy   = Memory::WY;
	    wx   = Memory::WX;
	}

	void put_pallete(byte pallete[4], byte value)
	{
	    pallete[0] = (value & 0x03) >> 0;
	    pallete[1] = (value & 0x0C) >> 2;
	    pallete[2] = (value & 0x30) >> 4;
	    pallete[3] = (value & 0xC0) >> 6;
	}

	void write_pallete(PalleteType pallete, byte value)
	{
	    switch (pallete)
	    {
		case PalleteType::SPRITE_1: put_pallete(sprite_pallete1,    value); break;
		case PalleteType::SPRITE_2: put_pallete(sprite_pallete2,    value); break;
		case PalleteType::BGROUND : put_pallete(background_pallete, value); break;
	    }
	}
    }
}
